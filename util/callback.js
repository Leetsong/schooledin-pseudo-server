const colors = require('colors');

colors.setTheme({
	debug: 'magenta',
});

function error(callback, m) {
	console.log(`[at ${new Date().toLocaleString()} in ${arguments.callee.caller.name ? arguments.callee.caller.name : '_'}]: ${m}`.debug);
	callback.apply(null, Array.prototype.slice.call(arguments, 1));
}

function normal(callback) {
	callback.apply(null, Array.prototype.slice.call(arguments, 1));
}

module.exports = {
	e: error,
	n: normal
}