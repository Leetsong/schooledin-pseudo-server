var colors = require('colors');

const VERBOSE = 1;
const DEBUG = 2;
const INFO = 3;
const WARN = 4;
const ERROR = 5;
const NOTHING = 6;

const level = DEBUG;

colors.setTheme({
	verbose: 'white',
	debug: 'magenta',
	info: 'green',
	warn: 'yellow',
	error: 'red'
});

function verbose(m) {
	if(level <= VERBOSE) {
		console.log(`[at ${new Date().toLocaleString()} in ${arguments.callee.caller.name ? arguments.callee.caller.name : '_'}]: ${m}`.verbose);
	}
}

function debug(m) {
	if(level <= DEBUG) {
		console.log(`[at ${new Date().toLocaleString()} in ${arguments.callee.caller.name ? arguments.callee.caller.name : '_'}]: ${m}`.debug);
	}
}

function info(m) {
	if(level <= INFO) {
		console.log(`[at ${new Date().toLocaleString()} in ${arguments.callee.caller.name ? arguments.callee.caller.name : '_'}]: ${m}`.info);
	}
}

function warn(m) {
	if(level <= WARN) {
		console.log(`[at ${new Date().toLocaleString()} in ${arguments.callee.caller.name ? arguments.callee.caller.name : '_'}]: ${m}`.warn);
	}
}

function error(m) {
	if(level <= ERROR) {
		console.log(`[at ${new Date().toLocaleString()} in ${arguments.callee.caller.name ? arguments.callee.caller.name : '_'}]: ${m}`.error);
	}
}

module.exports = {
	v: verbose,
	d: debug,
	i: info,
	w: warn,
	e: error
}