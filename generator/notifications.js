module.exports = function(BASE_URL) {

const fs = require('fs');
const path = require('path');
const _ = require('underscore');
const CONST = require('./const.js');
const users = require('../jsondb/users.json');
const questions = require('../jsondb/questions.json');
const answers = require('../jsondb/answers.json');
const essays = require('../jsondb/essays.json'); 

function InvitedNotification(id, asker, question) {
	return {
    "id": id,
    "type": "invited",
    "has_read": false,
    "asker_name": asker.name,
    "question_title": question.title,
    "asker_url": asker.url,
    "question_url": question.url
	};
}

function AnsweredNotification(id, answerer, question) {
	return {
    "id": id,
    "type": "answered",
    "has_read": false,
    "answerer_name": answerer.name,
    "question_title": question.title,
    "answerer_url": answerer.url,
    "question_url": question.url
	};
}

function CommentedNotification(id, parent_type, parent) {
	return {
    "id": id,
    "type": "commented",
    "has_read": false,
    "parent_type": parent_type,
    "parent_url": parent.url,
	};
}

var notifications = {}; // userId:notifications

for(var i = 0; i < users.length; i ++) {
	var user = users[i];
	var ns = [];

	var num = _.random(0, CONST.LIMITED_NOTIFICATIONS - 1);
	for(var j = 0; j < num; j ++) {
		var index = _.random(0, 2);
		var n = null;

		switch(index) {
			case 0: 
				n = InvitedNotification(j, 
					users[_.random(0, users.length - 1)],
					questions[_.random(0, questions.length - 1)]); 
				break;
			case 1: 
				n = AnsweredNotification(j,
					users[_.random(0, users.length - 1)],
					questions[_.random(0, questions.length - 1)]); 
				break;
			case 2: 
				var parent_type = _.random(0, 1) == 0 ? "essay" : "answer";
				n = CommentedNotification(j,
					parent_type,
					parent_type == "essay" ? 
						essays[_.random(0, essays.length - 1)] : 
						answers[_.random(0, answers.length - 1)]); 
				break;
		}

		if(n) {
			ns.push(n);
		}
	}

	notifications[user.id] = ns;
}

var fileName = path.join(__dirname, "../jsondb/notifications.json");
fs.writeFileSync(fileName, JSON.stringify(notifications));

}