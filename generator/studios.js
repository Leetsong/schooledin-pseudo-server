module.exports = function(BASE_URL) {

const fs = require('fs');
const path = require('path');
const _ = require('underscore');
const CONST = require('./const.js');
const users = require('../jsondb/users.json');

var nameList = [
	"Aaron", "Abbott",
	"Barry", "Bart",
	"Caesar", "Calvin",
	"David", "Dean",
	"Eric", "Elton",
	"Ford", "Frank",
	"Glenn", "Greg",
	"Harry", "Hayden",
	"Ira", "Isaac",
	"John", "Jonas",
	"Kit", "Kim",
	"Larry", "Len",
	"Marico", "Marlon",
	"Norman", "Noel",
	"Oliver", "Oscar",
	"Philip", "Peter",
	"Quinn", "Quincy",
	"Rachel", "Regan",
	"Simon", "Sam",
	"Ted", "Tim",
	"Ulysses", "Uriah",
	"Vic", "Victor",
	"Water", "Will",
	"Xavier", "X",
	"Yale", "York",
	"Zed", "Ziv",
	"计算机科学与技术系",
	"软件学院", 
	"电子科学与技术学院",
	"地球与海洋科学学院",
	"社会学院",
	"商学院",
	"工程管理学院",
	"法学院",
	"大气科学学院",
	"环境学院",
	"匡亚明学院",
	"化学院",
	"天文与空间科学学院",
	"物理学院",
	"数学系",
	"文学院",
	"医学院",
	"新闻传播学院"
];

var studios = [];

for (var id = 0; id < CONST.NR_STUDIO; id ++) {
	var manager = users[_.random(0, CONST.NR_USER - 1)];
	studios.push({
		"id": id,
	  "name": nameList[_.random(0, nameList.length - 1)] + ((_.random(0, 1000 - 1) % 2 == 0) ? "工作室" : "小组"),
	  "manager": manager.name,
	  "members": _.random(1, CONST.LIMITED_USERS - 1),
	  "questions": _.random(0, CONST.LIMITED_QUESTIONS - 1),
	  "essays": _.random(0, CONST.LIMITED_ESSAYS - 1),
	  "url": BASE_URL + "/api/studios/" + id,
	  "avatar_url": BASE_URL + "/avatars/" + _.random(0, CONST.NR_AVATAR - 1) + '.png',
	  "manager_url": manager.url,
	  "manager_avatar_url": manager.avatar_url,
	  "members_url": BASE_URL + "/api/studios/" + id + "/members",
	  "questions_url": BASE_URL + "/api/studios/" + id + "/questions",
	  "essays_url": BASE_URL + "/api/studios/" + id + "/essays",
		"top_question_url": BASE_URL + "/api/questions/" + _.random(0, CONST.NR_QUESTION - 1),
		"top_essay_url": BASE_URL + "/api/essays/" + _.random(0, CONST.NR_ESSAY - 1),
		"bio": "欢迎参加我们！"
	});
}

var fileName = path.join(__dirname, "../jsondb/studios.json");
fs.writeFileSync(fileName, JSON.stringify(studios));

}