module.exports = function(BASE_URL) {

const fs = require('fs');
const path = require('path');
const _ = require('underscore');
const CONST = require('./const.js');
const studios = require('../jsondb/studios.json');

var titleList = [
	"脚手架类的命令行工具用到了哪些技术?",
	"什么时候你不能使用箭头函数？",
	"有哪些看似简单其实非常精妙的代码？",
	"推送的演变",
	"当一个颜值很高的程序员是怎样一番体验？",
	"维护一个大型开源项目是怎样的体验？",
	"本人高一学生，想抽空学习编程(0基础)，想请教各位大神意见或经历分享。感谢！？",
	"IOS平台TensorFlow实践有什么？",
	"你见过最震撼的自然景观是什么？",
	"你们看过的最毁三观的作品是什么?",
	"放弃一个很爱的人是什么感觉？",
	"放弃一个很爱的人是什么感觉？",
	"程序员每天会阅读哪些技术网站或者公众号来提高能力？",
	"科技大佬在严格控制孩子玩手机？",
	"为什么体育竞技中客场对球员影响如此大？",
	"如何设计并实现一个通用的应用运维管控平台",
	"有哪些圈子里才知道的小秘密？",
	"我们为什么选择用 Python 来开发 Quora",
	"为什么我将知乎文章保存在本地后不能打开?",
	"《冰与火之歌》有什么细思恐极的情节？"
];

var contentList = [
	"自己做的 NodeJS 项目，想实现像 Express 那样 $express myapp 就自动生成了项目结构和相关文件，请问都需要用到哪些技术？\n\n或者像 VUE的命令行工具 vue init webpack my-project  那样的效果也行。\n\n如果能够简单科普一下细节就更感谢了。",
	"共 2670 字，读完需 5 分钟。编译自 Dmitri Pavlutin 的文章，对原文内容做了精简和代码风格优化。ES6 中引入的箭头函数可以让我们写出更简洁的代码，但是部分场景下使用箭头函数会带来严重的问题，有哪些场景？会导致什么问题？该怎么解决，容我慢慢道来。",
	"c c++ java python object-c java script  Visual Basic PHP 汇编\nMATLAB。。。。。",
	"能见证每天在用的编程语言不断演化是一件让人非常兴奋的事情，从错误中学习、探索更好的语言实现、创造新的语言特性是推动编程语言版本迭代的动力。JS 近几年的变化就是最好的例子， 以 ES6 引入的箭头函数（arrow functions）、class 等特性为代表，把 JS 的易用性推到了新的高度。",
	"关于 ES6 中的箭头函数，网上有很多文章解释其作用和语法，如果你刚开始接触 ES6，可以从这里开始。任何事物都具有两面性，语言的新特性常常被误解、滥用，比如箭头函数的使用就存在很多误区。接下来，笔者会通过实例介绍该避免使用箭头函数的场景，以及在这些场景下该如何使用函数表达式（function expressions）、函数声明或者方法简写（shorthand method）来保障代码正确性和可读性。",
	"JS 中对象方法的定义方式是在对象上定义一个指向函数的属性，当方法被调用的时候，方法内的 this 就会指向方法所属的对象\n因为箭头函数的语法很简洁，可能不少同学会忍不住用它来定义字面量方法",
	"calculator.sum 使用箭头函数来定义，但是调用的时候会抛出 TypeError，因为运行时 this.array 是未定义的，调用 calculator.sum 的时候，执行上下文里面的 this 仍然指向的是 window，原因是箭头函数把函数上下文绑定到了 window 上，this.array 等价于 window.array，显然后者是未定义的。",
	"同样的规则适用于原型方法（prototype method）的定义，使用箭头函数会导致运行时的执行上下文错误",
	"this 是 JS 中很强大的特性，可以通过多种方式改变函数执行上下文，JS 内部也有几种不同的默认上下文指向，但普适的规则是在谁上面调用函数 this 就指向谁，这样代码理解起来也很自然，读起来就像在说，某个对象上正在发生某件事情。",
	"但是，箭头函数在声明的时候就绑定了执行上下文，要动态改变上下文是不可能的，在需要动态上下文的时候它的弊端就凸显出来。比如在客户端编程中常见的 DOM 事件回调函数（event listenner）绑定，触发回调函数时 this 指向当前发生事件的 DOM 节点，而动态上下文这个时候就非常有用",
	"在全局上下文下定义的箭头函数执行时 this 会指向 window，当单击事件发生时，浏览器会尝试用 button 作为上下文来执行事件回调函数，但是箭头函数预定义的上下文是不能被修改的，这样 this.innerHTML 就等价于 window.innerHTML，而后者是没有任何意义的。",
	"构造函数中的 this 指向新创建的对象，当执行 new Car() 的时候，构造函数 Car 的上下文就是新创建的对象，也就是说 this instanceof Car === true。显然，箭头函数是不能用来做构造函数， 实际上 JS 会禁止你这么做，如果你这么做了，它就会抛出异常。",
	"构造新的 Message 实例时，JS 引擎抛了错误，因为 Message 不是构造函数。在笔者看来，相比旧的 JS 引擎在出错时悄悄失败的设计，ES6 在出错时给出具体错误消息是非常不错的实践。",
	"箭头函数允许你省略参数两边的括号、函数体的花括号、甚至 return 关键词，这对编写更简短的代码非常有帮助。这让我想起大学计算机老师给学生留过的有趣作业：看谁能使用 C 语言编写出最短的函数来计算字符串的长度，这对学习和探索新语言特性是个不错的法子。但是，在实际的软件工程中，代码写完之后会被很多工程师阅读，真正的 write once, read many times，在代码可读性方面，最短的代码可能并不总是最好的。一定程度上，压缩了太多逻辑的简短代码，阅读起来就没有那么直观",
	"multiply 函数会返回两个数字的乘积或者返回一个可以继续调用的固定了一个参数的函数。代码看起来很简短，但大多数人第一眼看上去可能无法立即搞清楚它干了什么，怎么让这段代码可读性更高呢？有很多办法，可以在箭头函数中加上括号、条件判断、返回语句，或者使用普通的函数",
	"箭头函数无疑是 ES6 带来的重大改进，在正确的场合使用箭头函数能让代码变的简洁、短小，但某些方面的优势在另外一些方面可能就变成了劣势，在需要动态上下文的场景中使用箭头函数你要格外的小心，这些场景包括：定义对象方法、定义原型方法、定义构造函数、定义事件回调函数。",
	"本文作者王仕军，商业转载请联系作者获得授权，非商业转载请注明出处。如果你觉得本文对你有帮助，请点赞！如果对文中的内容有任何疑问，欢迎留言讨论。想知道我接下来会写些什么？欢迎订阅我的知乎专栏：《前端周刊：让你在前端领域跟上时代的脚步》。"
];

var essays = [];

for (var id = 0; id < CONST.NR_ESSAY; id ++) {
	var studio = studios[_.random(0, CONST.NR_STUDIO - 1)];
	var r = _.random(0, 10 - 1);
	var content = "";

	for(var i = 0; i < r; i ++) {
		content += '\n\n' + contentList[_.random(0, contentList.length - 1)];
	}

	essays.push({
		"id": id,
	  "title": titleList[_.random(0, titleList.length - 1)],
	  "content": content,
	  "heat": _.random(0, 100 - 1),
	  "supports": _.random(0, CONST.NR_USER - 1),
	  "comments": _.random(0, CONST.LIMITED_COMMENTS - 1),
	  "studio": studio.name,
	  "studio_bio": studio.bio,
	  "url": BASE_URL + "/api/essays/" + id,
	  "studio_url": studio.url,
	  "studio_avatar_url": studio.avatar_url,
	  "comments_url": BASE_URL + "/api/essays/" + id + "/comments",
	  "created_at": Date.now() + _.random(0, 10000 - 1)
	});
}

var fileName = path.join(__dirname, "../jsondb/essays.json");
fs.writeFileSync(fileName, JSON.stringify(essays));

}