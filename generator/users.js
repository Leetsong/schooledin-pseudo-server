module.exports = function(BASE_URL) {

const fs = require('fs');
const path = require('path');
const _ = require('underscore');
const CONST = require('./const.js');

var nameList = [
	"Aaron", "Abbott",
	"Barry", "Bart",
	"Caesar", "Calvin",
	"David", "Dean",
	"Eric", "Elton",
	"Ford", "Frank",
	"Glenn", "Greg",
	"Harry", "Hayden",
	"Ira", "Isaac",
	"John", "Jonas",
	"Kit", "Kim",
	"Larry", "Len",
	"Marico", "Marlon",
	"Norman", "Noel",
	"Oliver", "Oscar",
	"Philip", "Peter",
	"Quinn", "Quincy",
	"Rachel", "Regan",
	"Simon", "Sam",
	"Ted", "Tim",
	"Ulysses", "Uriah",
	"Vic", "Victor",
	"Water", "Will",
	"Xavier", "X",
	"Yale", "York",
	"Zed", "Ziv"
];

var departmentList = [
	"计算机科学与技术系",
	"软件学院", 
	"电子科学与技术学院",
	"地球与海洋科学学院",
	"社会学院",
	"商学院",
	"工程管理学院",
	"法学院",
	"大气科学学院",
	"环境学院",
	"匡亚明学院",
	"化学院",
	"天文与空间科学学院",
	"物理学院",
	"数学系",
	"文学院",
	"医学院",
	"新闻传播学院"
];

var users = [];

for (var email = 141220000; email < 141220000 + CONST.NR_USER; email ++) {
	var id = email - 141220000;
	users.push({
		"id": id,
	  "password": "12345+-*/=",
	  "name": nameList[_.random(0, nameList.length - 1)],
	  "answers": _.random(0, CONST.LIMITED_ANSWERS - 1),
	  "questions": _.random(0, CONST.LIMITED_QUESTIONS - 1),
	  "favorite_answers": _.random(0, CONST.LIMITED_ANSWERS - 1),
	  "favorite_questions": _.random(0, CONST.LIMITED_QUESTIONS - 1),
	  "favorite_essays": _.random(0, CONST.LIMITED_ESSAYS - 1),
	  "studios": _.random(0, CONST.LIMITED_STUDIOS - 1),
	  "url": BASE_URL + "/api/users/" + id,
	  "avatar_url": BASE_URL + "/avatars/" + _.random(0, CONST.NR_AVATAR - 1) + '.png',
	  "questions_url": BASE_URL + "/api/users/" + id + "/questions",
	  "answers_url": BASE_URL + "/api/users/" + id + "/answers",
	  "favorite_questions_url": BASE_URL + "/api/users/" + id + "/favorites/questions",
	  "favorite_answers_url": BASE_URL + "/api/users/" + id + "/favorites/answers",
	  "favorite_essays_url": BASE_URL + "/api/users/" + id + "/favorites/essays",
	  "studios_url": BASE_URL + "/api/users/" + id + "/studios",
	  "email": email + "@smail.nju.edu.cn",
	  "phone": String(13276622706 + id),
	  "bio": "哈哈哈",
	  "sex": _.random(0, 1000 - 1) % 2 == 0 ? "male" : "female",
	  "age": _.random(0, 42 - 1),
	  "department": departmentList[_.random(0, departmentList.length - 1)],
	  "location": "南京"		
	});
}

var fileName = path.join(__dirname, "../jsondb/users.json");
fs.writeFileSync(fileName, JSON.stringify(users));

}