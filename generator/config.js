const CONST = require('./const.js');

module.exports = {
	// IP address of the generated data
	'ipaddr': CONST.DEFAULT_IP,

	// PORT of the generated data
	'port': CONST.DEFAULT_PORT
};