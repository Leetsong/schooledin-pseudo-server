const express = require('express');
const bodyParser = require('body-parser');
const log = require('./util/log');
const httpInterceptor = require('./interceptor');
const apiIndex = require('./router');
const apiUsers = require('./router/users.js');
const apiStudios = require('./router/studios.js');
const apiComments = require('./router/comments.js');
const apiAnswers = require('./router/answers.js');
const apiQuestions = require('./router/questions.js');
const apiEssays = require('./router/essays.js');
const ApiError = require('./router/error.js');

// https
const https = require('https');
const credentials = require('./ssl');

const config = require('./jsondb/config.json');
const ipaddr = config.ipaddr;
const port = config.port;
var app = express();
var server = https.createServer(credentials, app);

// body 解析器
app.use(bodyParser.json());

// http 拦截器
app.use(httpInterceptor);

// 路由
app.use(apiIndex.base, apiIndex.router);
app.use(apiUsers.base, apiUsers.router);
app.use(apiStudios.base, apiStudios.router);
app.use(apiQuestions.base, apiQuestions.router);
app.use(apiEssays.base, apiEssays.router);
app.use(apiAnswers.base, apiAnswers.router);
app.use(apiComments.base, apiComments.router);

// 静态资源
app.use(express.static('public'));

// 出错处理
app.use(function(req, res) {
	res.status(404).json(ApiError(404));
});

// 启动
server.listen(port, ipaddr, function() {
	log.i('pseudo-server started on ' + config.baseUrl + '\n');
});