const express = require('express');
const ApiError = require('./error.js');
const Users = require('../db').users();
const Comments = require('../db').comments();
const BASE_URL = "/api/comments";

var router = express.Router();

router.get('/:commentId', function(req, res) {
	var commentId = parseInt(req.params.commentId);
	Comments.findOne(commentId, function(err, comment) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(comment);
		}
	});
});

router.post('/', function(req, res) {
	Users.checkUser(req.body.commenter_email_or_phone, req.body.commenter_password, function(err, user) {
		if(err) {
			res.status(401).json(ApiError(401));
		} else {
			var c = req.body;
			Comments.saveOne(c, user, function(err, comment) {
				if(err) {
					res.status(503).json(ApiError(503));
				} else {
					res.status(201).json({
						'url': BASE_URL + "/" + comment.id
					});
				}
			});
		}
	});
});

module.exports = {
	base: BASE_URL,
	router: router
};