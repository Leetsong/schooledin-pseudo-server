const express = require('express');
const _ = require('underscore');
const ApiError = require('./error.js');
const Users = require('../db').users();
const Questions = require('../db').questions();
const BASE_URL = "/api/questions";

var router = express.Router();

router.get('/', function(req, res) {
	var offset = req.query.offset || undefined;
	var limit = req.query.limit || undefined;
	var search = req.query.search || undefined;

	if(!offset || !limit) {
		res.status(400).json(ApiError(400));
	} else {
		var ids = undefined;

		offset = parseInt(offset);
		limit = parseInt(limit);
		ids = _.range(offset, offset+limit);

		Questions.find(ids, function(err, questions) {
			if(err) {
				res.status(503).json(ApiError(503))
			} else {
				res.status(200).json(questions);
			}
		});
	}
});

router.get('/:questionId', function(req, res) {
	var questionId = parseInt(req.params.questionId);
	Questions.findOne(questionId, function(err, question) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(question);
		}
	});
});

router.get('/:questionId/answers', function(req, res) {
	var questionId = parseInt(req.params.questionId);
	Questions.findAnswers(questionId, function(err, answers) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(answers);
		}
	});
});

router.post('/', function(req, res) {
	Users.checkUser(req.body.asker_email_or_phone, req.body.asker_password, function(err, user) {
		if(err) {
			res.status(401).json(ApiError(401));
		} else {
			var q = req.body;
			Questions.saveOne(q, user, function(err, question) {
				if(err) {
					res.status(503).json(ApiError(503));
				} else {
					res.status(201).json({
						'url': BASE_URL + "/" + question.id
					});
				}
			});
		}
	});
})

module.exports = {
	base: BASE_URL,
	router: router
};