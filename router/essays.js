const express = require('express');
const _ = require('underscore');
const ApiError = require('./error.js');
const Users = require('../db').users();
const Essays = require('../db').essays();
const BASE_URL = "/api/essays";

var router = express.Router();

router.get('/', function(req, res) {
	var offset = req.query.offset || undefined;
	var limit = req.query.limit || undefined;
	var search = req.query.search || undefined;

	if(!offset || !limit) {
		res.status(400).json(ApiError(400));
	} else {
		var ids = undefined;

		offset = parseInt(offset);
		limit = parseInt(limit);
		ids = _.range(offset, offset+limit);

		Essays.find(ids, function(err, essays) {
			if(err) {
				res.status(503).json(ApiError(503))
			} else {
				res.status(200).json(essays);
			}
		});
	}
});

router.get('/:essayId', function(req, res) {
	var essayId = parseInt(req.params.essayId);
	Essays.findOne(essayId, function(err, essay) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(essay);
		}
	});
});

router.get('/:essayId/comments', function(req, res) {
	var essayId = parseInt(req.params.essayId);
	Essays.findComments(essayId, function(err, comments) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(comments);
		}
	});
});

router.post('/', function(req, res) {
	Users.checkUser(req.body.studio_manager_email_or_phone, req.body.studio_manager_password, function(err, user) {
		if(err) {
			res.status(401).json(ApiError(401));
		} else {
			var e = req.body;
			Essays.saveOne(e, function(err, essay) {
				if(err) {
					res.status(503).json(ApiError(503));
				} else {
					res.status(201).json({
						'url': BASE_URL + "/" + essay.id
					});
				}
			});
		}
	});
});

module.exports = {
	base: BASE_URL,
	router: router
};