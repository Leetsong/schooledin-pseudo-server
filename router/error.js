const _ = require('underscore');

function ApiError(m) {
	var r = {
		message: null,
		api_url: '/api'
	};

	if(_.isString(m)) {
		r.message = m;
		return r;
	} else if(_.isNumber(m)) {
		switch(m) {
			case 400: r.message = 'Lack of queries: offset or limit'; break;
			case 401: r.message = 'Invalid email/phone or password'; break;
			case 404: r.message = 'Not found'; break;
			case 409: r.message = 'Email has already been signed up'; break;
			case 503: r.message = 'Server is busy'; break;
		}
		return r;
	}
};

module.exports = ApiError;