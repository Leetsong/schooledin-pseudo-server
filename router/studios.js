const express = require('express');
const _ = require('underscore');
const ApiError = require('./error.js');
const Users = require('../db').users();
const Studios = require('../db').studios();
const BASE_URL = "/api/studios";

var router = express.Router();

router.get('/', function(req, res) {
	var offset = req.query.offset || undefined;
	var limit = req.query.limit || undefined;
	var search = req.query.search || undefined;

	if(!offset || !limit) {
		res.status(400).json(ApiError(400));
	} else {
		var ids = undefined;

		offset = parseInt(offset);
		limit = parseInt(limit);
		ids = _.range(offset, offset+limit);

		Studios.find(ids, function(err, studios) {
			if(err) {
				res.status(503).json(ApiError(503))
			} else {
				res.status(200).json(studios);
			}
		});
	}
});

router.get('/:studioId', function(req, res) {
	var studioId = parseInt(req.params.studioId);
	Studios.findOne(studioId, function(err, studio) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(studio);
		}
	});
});

router.get('/:studioId/members', function(req, res) {
	var studioId = parseInt(req.params.studioId);
	Studios.findMembers(studioId, function(err, members) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(members);
		}
	});
});

router.get('/:studioId/questions', function(req, res) {
	var studioId = parseInt(req.params.studioId);
	Studios.findQuestions(studioId, function(err, questions) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(questions);
		}
	});
});

router.get('/:studioId/essays', function(req, res) {
	var studioId = parseInt(req.params.studioId);
	Studios.findEssays(studioId, function(err, essays) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(essays);
		}
	});
});

router.post('/', function(req, res) {
	Users.checkUser(req.body.manager_email_or_phone, req.body.manager_password, function(err, user) {
		if(err) {
			res.status(401).json(ApiError(401));
		} else {
			var s = req.body;
			Studios.saveOne(s, user, function(err, studio) {
				if(err) {
					res.status(503).json(ApiError(503));
				} else {
					res.status(201).json({
						'url': BASE_URL + "/" + studio.id
					});
				}
			});
		}
	});
});

router.patch('/:studioId', function(req, res) {
	var studioId = parseInt(req.params.studioId);
	var ns = req.body;

	Studios.findOne(studioId, function(err, studio) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			Users.checkUser(ns.manager_email_or_phone, ns.manager_password, function(err, user) {
				if(err || user.name !== studio.manager) {
					res.status(401).json(ApiError(401));
				} else {
					var s = _.pick(ns, 
						'new_manager_email_or_phone', 'new_manager_password', 
						'bio', 'avatar', 'members', 'top_question', 'top_essay'
					);

					if(s.new_manager_email_or_phone) {
						Users.checkUser(s.new_manager_email_or_phone, s.new_manager_password, function(err, newManager) {
							if(err) {
								res.status(401).json(ApiError(401));
							} else {
								delete s.new_manager_email_or_phone;
								delete s.new_manager_password;
								s.manager = newManager.name;
								s.newManager = newManager;
								Studios.updateOne(studio, s, function(err, id) {
									if(err) {
										res.status(503).json(ApiError(503));
									} else {
										res.status(204).send();
									}
								});								
							}
						});
					} else {
						Studios.updateOne(studio, s, function(err, id) {
							if(err) {
								res.status(503).json(ApiError(503));
							} else {
								res.status(204).send();
							}
						});
					}
				}
			});
		}
	});
});

module.exports = {
	base: BASE_URL,
	router: router
};