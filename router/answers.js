const express = require('express');
const ApiError = require('./error.js');
const Users = require('../db').users();
const Answers = require('../db').answers();
const BASE_URL = "/api/answers";

var router = express.Router();

router.get('/:answerId', function(req, res) {
	var answerId = parseInt(req.params.answerId);
	Answers.findOne(answerId, function(err, answer) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(answer);
		}
	});
});

router.get('/:answerId/comments', function(req, res) {
	var answerId = parseInt(req.params.answerId);
	Answers.findComments(answerId, function(err, comments) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(comments);
		}
	});
});

router.post('/', function(req, res) {
	Users.checkUser(req.body.answerer_email_or_phone, req.body.answerer_password, function(err, user) {
		if(err) {
			res.status(401).json(ApiError(401));
		} else {
			var a = req.body;
			Answers.saveOne(a, user, function(err, answer) {
				if(err) {
					res.status(503).json(ApiError(503));
				} else {
					res.status(201).json({
						'url': BASE_URL + "/" + answer.id
					});
				}
			});
		}
	});
});

module.exports = {
	base: BASE_URL,
	router: router
};