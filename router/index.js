var router = require('express').Router();
const _ = require('underscore');
const SessionTab = require('./session.js')
const ApiError = require('./error.js');
const Users = require('../db').users();
const BASE_URL = "/api";

var router = require('express').Router();

router.get('/', function(req, res) {
	var ret = {
	  "url": "/api",
	  "users_url": "/api/users?offset={offset}&limit={limit}{&search}",
	  "user_url": "/api/users/{userId}",
	  "studios_url": "/api/studios?offset={offset}&limit={limit}{&search}",
	  "studio_url": "/api/studios/{studioId}",
	  "questions_url": "/api/questions?offset={offset}&limit={limit}{&search,heat}",
	  "question_url": "/api/questions/{questionId}",
	  "essays_url": "/api/essays?offset={offset}&limit={limit}{&search,heat}",
	  "essay_url": "/api/essays/{essayId}"
	};

	res.status(200).json(ret);
});

router.post('/login', function(req, res) {
	var u = req.body;
	var option = Users.extractAccountOption(u.user_email_or_phone);

	if(!option) {
		res.status(400).json(ApiError('Invalid NJU email or phone'));
		return ;
	}

	Users.findOneBy(option, function(err, user) {
		if(err) {
			res.status(401).json(ApiError(401));
		} else {
			if(u.user_password !== user.password) {
				res.status(401).json(ApiError(401));
			} else {
				var pendingSession = SessionTab.newPendingSession(user);
				res.status(200).json(pendingSession.getFullUser());
			}
		}
	});
});

router.post('/login/confirm', function(req, res) {
	var u = req.body;

	Users.findOne(u.user_id, function(err, user) {
		if(err) {
			res.status(400).json(ApiError("You have to do login first, then to do login/confirm"));
		} else {
			var pendingSession = SessionTab.getPendingSession(user.id, u.alias, u.tags);
			if (pendingSession) {
				SessionTab.convertToSession(pendingSession);
				res.status(201).send();
			} else {
				res.status(400).json(ApiError("You have to do login first, then to do login/confirm"));
			}
		}
	});
});

router.post('/logout', function(req, res) {
	var u = req.body;
	var option = Users.extractAccountOption(u.user_email_or_phone);

	if(!option) {
		res.status(400).json(ApiError('Invalid NJU email or phone'));
		return ;
	}

	Users.findOneBy(option, function(err, user) {
		if(err) {
			res.status(401).json(ApiError(401));
		} else {
			if(u.user_password !== user.password) {
				res.status(401).json(ApiError(401));
			} else {
				var session = SessionTab.getSession(user.id);
				if (session) {
					SessionTab.removeSession(session);
					res.status(204).send();
				} else {
					res.status(400).json(ApiError("You have to do login first"));
				}
			}
		}
	});
});

router.get("/index", function(req, res) {
	var offset = req.query.offset || undefined;
	var limit = req.query.limit || undefined;

	if(!offset || !limit) {
		res.status(400).json(ApiError(400));
	} else {
		var ids = undefined;

		offset = parseInt(offset);
		limit = parseInt(limit);

		if (offset > 100) {
			res.status(200).json([]);
		}

		ids = _.range(offset, offset+limit);

		require('../db').essays().find(ids, function(err, essays) {
			if(err) {
				res.status(503).json(ApiError(503))
			} else {
				for(var i = 0; i < essays.length; i ++) {
					essays[i].type = 'essay';
				}
				require('../db').questions().find(ids, function(err, questions) {
					if (err) {
						res.status(200).json(essays);						
					} else {
						for(var i = 0; i < questions.length; i ++) {
							questions[i].type = 'question';
						}
						var ret = essays.concat(questions);
						res.status(200).json(_.shuffle(ret).slice(0, limit));
					}
				});
			}
		});
	}
});

module.exports = {
	base: BASE_URL,
	router: router
}
