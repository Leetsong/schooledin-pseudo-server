const clone = require('clone');
const log = require('../util/log.js');

// Session

var Session = function(user) {
	this.__id = user.id;
	this.user = clone(user);
	this.alias = user.id + "";
	this.tags = [];
}

Session.prototype.getFullUser = function() {
	var u = clone(this.user);
	u.alias = this.alias;
	u.tags = this.tags;
	return u;
}

// SessionTab

function SessionTab() {
	this.__pendingSessionTab = {}; // hash table
	this.__sessionTab = {}; // hash table
}

SessionTab.prototype.newPendingSession = function(user) {
	var ps = new Session(user);
	this.__pendingSessionTab[user.id] = ps;

	log.d("New session [ " + user.id + " ] is pended");
	this.log();

	return ps;
};

SessionTab.prototype.getPendingSession = function(userId, alias, tags) {
	var ret = this.__pendingSessionTab[userId];

	if(!ret || ret.alias !== alias) {
		ret = null;
	}

	return ret;
};

SessionTab.prototype.convertToSession = function(pendingSession) {
	delete this.__pendingSessionTab[pendingSession.__id];
	this.__sessionTab[pendingSession.__id] = pendingSession;

	log.d("New session [ " + pendingSession.__id + " ] is created");
	this.log();
};

SessionTab.prototype.getSession = function(userId) {
	var ret = this.__sessionTab[userId];
	return ret;
};

SessionTab.prototype.removeSession = function(session) {
	delete this.__sessionTab[session.__id];

	log.d("A session [ " + session.__id + " ] is removed");
	this.log();
};

SessionTab.prototype.log = function() {
	var pst = {};
	for(var key in this.__pendingSessionTab) {
		pst[key] = {
			user_id: this.__pendingSessionTab[key].user.id,
			alias: this.__pendingSessionTab[key].alias,
			tags: this.__pendingSessionTab[key].tags
		};
	}

	var st = {};
	for(var key in this.__sessionTab) {
		st[key] = {
			user_id: this.__sessionTab[key].user.id,
			alias: this.__sessionTab[key].alias,
			tags: this.__sessionTab[key].tags
		};
	}

	log.d("PenddingSessionTab: " + JSON.stringify(pst, null, 2));
	log.d("SessionTab: " + JSON.stringify(st, null, 2));
}

var sessionTab = new SessionTab();

module.exports = sessionTab;