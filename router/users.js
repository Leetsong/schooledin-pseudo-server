const express = require('express');
const _ = require('underscore');
const crypto = require('crypto');
const log = require('../util/log.js');
const ApiError = require('./error.js');
// const Questions = require('../db').questions();
const Users = require('../db').users();
const BASE_URL = "/api/users";

var router = express.Router();

// ============= Verification Code =============

var verificationCodeCache = {};

function setVerificationCode(account) {
	if(verificationCodeCache[account]) return null;

	var md5 = crypto.createHash('md5');
	verificationCodeCache[account] = { };
	verificationCodeCache[account].code = 
		md5.update(account + Date.now()).digest('hex').slice(0, 5);
	verificationCodeCache[account].timer = setTimeout(function() {
		delete verificationCodeCache[account];

		log.i(
			'deleted verification code for ' + account);

	}, 60 * 1000 + 100);
	var now = Date.now();

	log.i(
		'saved verification code ' + verificationCodeCache[account].code + 
		' for ' + account + 
		', will be deleted in 1 min');

	return now + 60 * 1000;
}

function getVerificationCode(account) {
	var ret = null;
	if(verificationCodeCache[account]) {
		clearTimeout(verificationCodeCache[account].timer);
		ret = verificationCodeCache[account].code;
		delete verificationCodeCache[account];

		log.i(
			'deleted verification code for ' + account);

	} else {
		ret = null;
	}
	return ret;
}

// =============================================

router.get('/', function(req, res) {
	var offset = req.query.offset || undefined;
	var limit = req.query.limit || undefined;
	var search = req.query.search || undefined;

	if(!offset || !limit) {
		res.status(400).json(ApiError(400));
	} else {
		var ids = undefined;

		offset = parseInt(offset);
		limit = parseInt(limit);
		ids = _.range(offset, offset+limit);

		Users.find(ids, function(err, users) {
			if(err) {
				res.status(503).json(ApiError(503))
			} else {
				res.status(200).json(users);
			}
		});
	}
});

router.get('/:userId', function(req, res) {
	var userId = parseInt(req.params.userId);
	Users.findOne(userId, function(err, user) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(user);
		}
	});
});

router.get('/:userId/supports', function(req, res) {
	var offset = req.query.offset || undefined;
	var limit = req.query.limit || undefined;
	var search = req.query.search || undefined;

	if(!offset || !limit) {
		res.status(400).json(ApiError(400));
	} else {
		offset = parseInt(offset);
		limit = parseInt(limit);

		var userId = parseInt(req.params.userId);
		Users.findSupports(userId, limit, function(err, supports) {
			if(err) {
				res.status(404).json(ApiError(404));
			} else {
				res.status(200).json(supports);
			}
		});
	}
});

router.get('/:userId/favorites/questions', function(req, res) {
	var userId = parseInt(req.params.userId);
	Users.findFavoriteQuestions(userId, function(err, questions) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(questions);
		}
	});
});

router.get('/:userId/favorites/answers', function(req, res) {
	var userId = parseInt(req.params.userId);
	Users.findFavoriteAnswers(userId, function(err, answers) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(answers);
		}
	});
});

router.get('/:userId/favorites/essays', function(req, res) {
	var userId = parseInt(req.params.userId);
	Users.findFavoriteEssays(userId, function(err, essays) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(essays);
		}
	});
});

router.get('/:userId/questions', function(req, res) {
	var userId = parseInt(req.params.userId);
	Users.findQuestions(userId, function(err, questions) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(questions);
		}
	});
});

router.get('/:userId/answers', function(req, res) {
	var userId = parseInt(req.params.userId);
	Users.findAnswers(userId, function(err, answers) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(answers);
		}
	});
});

router.get('/:userId/studios', function(req, res) {
	var userId = parseInt(req.params.userId);
	Users.findStudios(userId, function(err, studios) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(studios);
		}
	});
});

router.get('/:userId/notifications', function(req, res) {
	var userId = parseInt(req.params.userId);
	Users.findNotifications(userId, function(err, notifications) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			res.status(200).json(notifications);
		}
	});
});

router.post('/requestVerifyEmail', function(req, res) {
	var option = Users.extractAccountOption(req.body.email);
	
	if(!option || !option.email) {
		res.status(400).json('Invalid NJU email address');
	} else {
		Users.findOneBy(option, function(err, user) {
			if(user) {
				res.status(409).json(ApiError(409));
			} else {
				var due = setVerificationCode(option.email);
				if(due) {
					res.status(201).json({
						"retain_until": due
					});
				} else {
					res.status(409).json(ApiError(409));
				}
			}
		});
	}
});

router.post('/requestVerifyPhone', function(req, res) {
	var option = Users.extractAccountOption(req.body.phone);
	
	if(!option || !option.phone) {
		res.status(400).json('Invalid NJU email address');
	} else {
		Users.findOneBy(option, function(err, user) {
			if(user) {
				res.status(409).json(ApiError(409));
			} else {
				var due = setVerificationCode(option.phone);
				if(due) {
					res.status(201).json({
						"retain_until": due
					});
				} else {
					res.status(409).json(ApiError(409));
				}
			}
		});
	}
});

router.post('/', function(req, res) {
	var user = req.body;
	var email = req.body.email;
	var password = req.body.password;
	var verificationCode = req.body.verification_code;

	if(verificationCode && verificationCode === getVerificationCode(email)) {
		Users.saveOne(user, function(err, u) {
			if(err) {
				res.status(503).json(ApiError(503));
			} else {
				res.status(201).json({
					'url': BASE_URL + '/' + u.id
				});
			}
		});
	} else {
		res.status(401).json(ApiError("Invalid validation code"));
	}
});

// router.post('/:userId/questions', function(req, res) {
// 	var id = req.param.userId;
// 	var q = req.body;
// 	Users.findOne(id, function(err, user) {
// 		if(err) {
// 			res.status(404).json(ApiError(404));
// 		} else {
// 			Users.checkUser(user.email, q.password, function(err, user) {
// 				if(err) {
// 					res.status(401).json(ApiError(401));
// 				} else {
// 					Questions.saveOne(q, user, function(err, question) {
// 						if(err) {
// 							res.status(503).json(ApiError(503));
// 						} else {
// 							res.status(201).json({
// 								"url": BASE_URL + '/' + question.id
// 							});
// 						}
// 					});
// 				}
// 			});
// 		}
// 	});
// });

router.patch('/:userId', function(req, res) {
	var userId = parseInt(req.params.userId);
	var nu = req.body;

	Users.findOne(userId, function(err, user) {
		if(err) {
			res.status(404).json(ApiError(404));
		} else {
			Users.checkUser(user.email, nu.password, function(err, user) {
				if(err) {
					res.status(401).json(ApiError(401));
				} else {
					var u = _.pick(nu, 
						'new_password', 'name', 'bio', 'age', 'sex', 
						'department', 'location', 'avatar', 'phone', 'verification_code'
					);

					if(u.new_password) {
						u.password = u.new_password;
						delete u.new_password;
					}
					
					if(nu.phone) {
						if(nu.verification_code && nu.verification_code === getVerificationCode(nu.phone)) {
							Users.updateOne(user, u, function(err, id) {
								if(err) {
									res.status(503).json(ApiError(503));
								} else {
									res.status(204).send();
								}
							});
						} else {
							res.status(401).json(ApiError(401));
						}
					} else {
						Users.updateOne(user, u, function(err, id) {
							if(err) {
								res.status(503).json(ApiError(503));
							} else {
								res.status(204).send();
							}
						});
					}
				}
			})
		}
	});
});

router.delete('/:userId/notifications', function(req, res) {
	var userId = parseInt(req.params.userId);
	var password = req.body.password;
	var notifications = req.body.notifications_id;

	Users.findOne(userId, function(err, user) {
		if(err) {
			res.status(204).send();
		} else {
			Users.checkUser(user.email, password, function(err, user) {
				if(err) {
					res.status(401).json(ApiError(401));
				} else {
					Users.setReadNotifications(userId, notifications, function(err) {
						if(err) {
							res.status(503).json(ApiError(503));
						} else {
							res.status(204).send();
						}
					});	
				}
			});
		}
	});
});

router.delete('/:userId/notifications/:notificationId', function(req, res) {
	var userId = parseInt(req.params.userId);
	var password = req.body.password;
	var notificationId = parseInt(req.params.notificationId);

	Users.findOne(userId, function(err, user) {
		if(err) {
			res.status(204).send();
		} else {
			Users.checkUser(user.email, password, function(err, user) {
				if(err) {
					res.status(401).json(ApiError(401));
				} else {
					Users.setReadNotification(userId, notificationId, function(err) {
						if(err) {
							res.status(503).json(ApiError(503));
						} else {
							res.status(204).send();
						}
					});	
				}
			});
		}
	});
});

module.exports = {
	base: BASE_URL,
	router: router
};