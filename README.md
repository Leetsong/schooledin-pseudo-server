# SchooledIn pseudo-server

> a pseudo-server of RESTful API, which returns fake data

## Usage

### download

```sh
git clone http://congli@114.212.189.169/SE-Android/pseudo-server.git
```

### install

``` sh
npm install
```

### generate certificate

Here, you can use `DNS:` and `IP:` to add your own SAN(Subject Alt Name) besides CN(Common Name) of your self-signed certificate

``` sh
npm run generate-cert -- [DNS:example.com] [IP:192.168.101] [...]
```

### generate fake data

Here, you can use `--ipaddr` to set the data's ip address, and `--port` to set port, `127.0.0.1` and `3000` are default for them respectively.

``` sh
npm run generate-data [-- --ipaddr=192.168.0.1 [--port=8080]]
```

### start

``` bsh
npm run pseudo-server
```

## Reference

+ [certify](https://github.com/rtts/certify/blob/master/certify)
+ [How to setup your own CA with OpenSSL](https://gist.github.com/Soarez/9688998)
+ [SSL](https://crsr.net/Notes/SSL.html)
+ [How are ssl certificate server names resolved can I add alternative names using keytool](https://stackoverflow.com/questions/8443081/how-are-ssl-certificate-server-names-resolved-can-i-add-alternative-names-using/8444863#8444863)
+ [javax.net.ssl.SSLPeerUnverifiedException: Hostname not verified](https://stackoverflow.com/questions/30745342/javax-net-ssl-sslpeerunverifiedexception-hostname-not-verified)
