const _ = require('underscore');
const log = require('../util/log.js');
const callback = require('../util/callback.js');
const config = require('../jsondb/config.json');
const userDb = require('../jsondb/users.json');
const notificationDb = require('../jsondb/notifications.json');
const db = require('./index.js');

var Users = { };

module.exports = Users;

function saveUserDb(savingDb) {
	require('fs').writeFileSync(
		require('path').join(__dirname, "../jsondb/users.json"), JSON.stringify(savingDb));
}

function saveNotificationsDb(savingDb) {
	require('fs').writeFileSync(
		require('path').join(__dirname, "../jsondb/notifications.json"), JSON.stringify(savingDb));
}

Users.extractAccountOption = function extractAccountOption(userEmailOrPhones) {
	var emailTest = /^\d{9}@(?:smail\.)?nju\.edu\.cn$/;
	var phoneTest = /^(?:13[0-9]|14[5|7]|15(?:[0-3]|[5-9])|18[0,5-9])\d{8}$/;

	var ret;

	if(_.isArray(userEmailOrPhones)) {
		ret = [];
		for(var i = 0; i < userEmailOrPhones.length; i ++) {
			if(emailTest.test(userEmailOrPhones[i])) {
				ret.push({ email: userEmailOrPhones[i] });
			} else if(phoneTest.test(userEmailOrPhones[i])) {
				ret.push({ phone: userEmailOrPhones[i] });
			} else {
				ret.push(null);
			}
		}
	} else {
		if(emailTest.test(userEmailOrPhones)) {
			ret = { email: userEmailOrPhones }
		} else if(phoneTest.test(userEmailOrPhones)) {
			ret = { phone: userEmailOrPhones }
		}	else {
			ret = null;
		}
	}

	return ret;
}

Users.checkUser = function checkUser(userEmailOrPhone, password, cb) {
	var option = Users.extractAccountOption(userEmailOrPhone);

	if(!option) { callback.e(cb, 'Invalid find option', null); }
	else {
		Users.findOneBy(option, function(err, user) {
			if(err) { callback.e(cb, 'User not found', null); }
			else if(password !== user.password){ callback.e(cb, 'Invalid password', null); }
			else { callback.n(cb, null, user); }
		});
	}
}

Users.findOneBy = function findOneBy(option, cb) {
	if(option.email) {
		for(var i = 0; i < userDb.length; i ++) {
			if(userDb[i].email == option.email) {
				callback.n(cb, null, userDb[i]);
				return ;
			}
		}
		callback.e(cb, "User not found", null);
	} else if(option.phone) {
		for(var i = 0; i < userDb.length; i ++) {
			if(userDb[i].phone == option.phone) {
				callback.n(cb, null, userDb[i]);
				return ;
			}
		}
		callback.e(cb, "User not found", null);
	} else {
		callback.e(cb, "Invalid option", null);
	}
}

Users.findBy = function findBy(options, cb) {
	var us = [];

	for(var i = 0; i < options.length; i ++) {
		var option = options[i];
		if(option && option.email) {
			for(var j = 0; j < userDb.length; j ++) {
				if(userDb[j].email == option.email) {
					us.push(userDb[j]);
				}
			}
		} else if(option && option.phone) {
			for(var j = 0; j < userDb.length; j ++) {
				if(userDb[j].phone == option.phone) {
					us.push(userDb[j]);
				}
			}
		}		
	}

	callback.n(cb, null, us);
}

Users.find = function find(ids, cb) {
	var users = [];
	for(var i = 0; i < ids.length; i ++) {
		if(ids[i] >= 0 && ids[i] < userDb.length) {
			var u = userDb[ids[i]];
			users.push({
				"id": u.id,
				"name": u.name,
				"answers": u.answers,
				"url": u.url,
				"avatar_url": u.avatar_url,
				"bio": u.bio
			});
		}
	}
	callback.n(cb, null, users);
}

Users.findOne = function findOne(id, cb) {
	if(id >= 0 && id < userDb.length) {
		callback.n(cb, null, _.omit(userDb[id], 'password'));
	} else {
		callback.e(cb, "User not found", null);
	}
}

Users.size = function size() {
	return userDb.length;
}

Users.findSupports = function findSupports(id, n, cb) {
	if(id >= 0 && id < userDb.length) {
		var ret = [];
		var questions = _.random(0, n - 1);
		var essays = n - questions;
		[].push.apply(ret, db.questions().randomN(questions));
		[].push.apply(ret, db.essays().randomN(essays));
		callback.n(cb, null, ret);
	} else {
		callback.e(cb, "User not found", null);
	}
}

Users.findFavoriteQuestions = function findFavoriteQuestions(id, cb) {
	if(id >= 0 && id < userDb.length) {
		callback.n(cb, null, db.questions().randomN(userDb[id].favorite_questions));
	} else {
		callback.e(cb, "User not found", null);
	}
}

Users.findFavoriteAnswers = function findFavoriteAnswers(id, cb) {
	if(id >= 0 && id < userDb.length) {
		callback.n(cb, null, db.answers().randomN(userDb[id].favorite_answers));
	} else {
		callback.e(cb, "User not found", null);
	}
}

Users.findFavoriteEssays = function findFavoriteEssays(id, cb) {
	if(id >= 0 && id < userDb.length) {
		callback.n(cb, null, db.essays().randomN(userDb[id].favorite_essays));
	} else {
		callback.e(cb, "User not found", null);
	}
}

Users.findQuestions = function findQuestions(id, cb) {
	if(id >= 0 && id < userDb.length) {
		callback.n(cb, null, db.questions().randomN(userDb[id].questions));
	} else {
		callback.e(cb, "User not found", null);
	}
}

Users.findAnswers = function findAnswers(id, cb) {
	if(id >= 0 && id < userDb.length) {
		callback.n(cb, null, db.answers().randomN(userDb[id].answers).map(function(a) {
			return _.omit(a, 'answerer', 'answerer_url', 'answerer_avatar_url');
		}));
	} else {
		callback.e(cb, "User not found", null);
	}
}

Users.findStudios = function findStudios(id, cb) {
	if(id >= 0 && id < userDb.length) {
		callback.n(cb, null, db.studios().randomN(userDb[id].studios));
	} else {
		callback.e(cb, "User not found", null);
	}
}

Users.findNotifications = function findNotifications(id, cb) {
	if(id >= 0 && id < userDb.length) {
		callback.n(cb, null, notificationDb[id]);
	} else {
		callback.e(cb, "User not found", null);
	}
}

Users.randomN = function randomN(n) {
	if(!n) return [];

	var ret = undefined;
	var retIds = _.sample(_.range(0, userDb.length - 1), n);
	Users.find(retIds, function(err, users) {
		ret = users;
	});

	return ret;
}

Users.saveOne = function saveOne(user, cb) {
	var id = userDb.length;
	var newUser = {
		"id": id,
		"password": user.password,
		"name": user.name ? user.name : "",
		"answers": 0,
		"questions": 0,
		"favorite_answers": 0,
		"favorite_questions": 0,
		"favorite_essays": 0,
		"studios": 0,
		"url": config.baseUrl + '/api/users/' + id,
		"avatar_url": user.avatar ? user.avatar : (config.baseUrl + '/avatars/44.png'),
		"questions_url": config.baseUrl + '/api/users/' + id + '/questions',
		"answers_url": config.baseUrl + '/api/users/' + id + '/answers',
		"favorite_questions_url": config.baseUrl + '/api/users/' + id + '/favorites/questions',
		"favorite_answers_url": config.baseUrl + '/api/users/' + id + '/favorites/answers',
		"favorite_essays_url": config.baseUrl + '/api/users/' + id + '/favorites/essays',
		"studios_url": config.baseUrl + '/api/users/' + id + '/favorites/studios',
		"email": user.email,
		"phone": user.phone ? user.phone : null,
		"bio": user.bio ? user.bio : "",
		"sex": user.sex ? user.sex : "unknown",
		"age": user.age ? user.age : 0,
		"department": user.department ? user.department : "",
		"location": user.location ? user.location : ""
	};

	userDb.push(newUser);
	saveUserDb(userDb);

	callback.n(cb, null, newUser);
}

Users.updateOne = function updateOne(user, options, cb) {
	var id = user.id;
	if(id >= 0 && id < userDb.length) {
		for(var key in options) {
			if(key in user) {
				userDb[id][key] = options[key];
			}
		}
		saveUserDb(userDb);
		callback.n(cb, null, userDb[id]);
		return ;
	}
	callback.e(cb, 'User not found', null);
}

Users.setReadNotifications = function setReadNotifications(userId, notificationsId, cb) {
	if(userId >= 0 && userId < userDb.length) {
		for(var i = 0; i < notificationsId.length; i ++) {
			if(notificationDb[userId][notificationsId[i]]) {
				notificationDb[userId][notificationsId[i]].has_read = true;
			}
		}
		saveNotificationsDb(notificationDb);
		callback.n(cb, null);
	}	 else {
		callback.e(cb, 'User not found');	
	}
}

Users.setReadNotification = function setReadNotifications(userId, notificationId, cb) {
	if(userId >= 0 && userId < userDb.length) {
		if(notificationDb[userId][notificationId]) {
			notificationDb[userId][notificationId].has_read = true;
		}
		saveNotificationsDb(notificationDb);
		callback.n(cb, null);
	}	 else {
		callback.e(cb, 'User not found');	
	}
}