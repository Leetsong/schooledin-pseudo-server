const _ = require('underscore');
const log = require('../util/log.js');
const callback = require('../util/callback.js');
const config = require('../jsondb/config.json');
const studioDb = require('../jsondb/studios.json');
const db = require('./index.js');

var Studios = { };

function saveStudioDb(savingDb) {
	require('fs').writeFileSync(
		require('path').join(__dirname, "../jsondb/studios.json"), JSON.stringify(savingDb));
}

Studios.findOneBy = function findOneBy(option, cb) {
	if(option.name) {
		for(var i = 0; i < studioDb.length; i ++) {
			if(studioDb[i].name == option.name) {
				callback.n(cb, null, studioDb[i]);
				return ;
			}
		}
		callback.e(cb, "Studio not found", null);
	} else {
		callback.e(cb, "Invalid find option", null);
	}
}

Studios.find = function find(ids, cb) {
	var studios = [];
	for(var i = 0; i < ids.length; i ++) {
		if(ids[i] >= 0 && ids[i] < studioDb.length) {
			studios.push(_.omit(
				studioDb[ids[i]], 
				'manager_avatar_url', 
				'members_url', 
				'questions_url', 'essays_url', 
				'top_question_url', 'top_essay_url'
			));
		}
	}
	callback.n(cb, null, studios);
}

Studios.findOne = function findOne(id, cb) {
	if(id >= 0 && id < studioDb.length) {
		callback.n(cb, null, studioDb[id]);
	} else {
		callback.e(cb, "Studio not found", null);
	}
}

Studios.findMembers = function findMembers(id, cb) {
	if(id >= 0 && id < studioDb.length) {
		callback.n(cb, null, db.users().randomN(studioDb[id].members));
	} else {
		callback.e(cb, "Studio not found", null);
	}
}

Studios.findQuestions = function findQuestions(id, cb) {
	if(id >= 0 && id < studioDb.length) {
		callback.n(cb, null, db.questions().randomN(studioDb[id].questions).map(function(q) {
			return {
				"id": q.id,
				"title": q.title,
				"heat": q.heat,
				"supports": q.supports,
				"answers": q.answers,
				"url": q.url,
				"answers_url": q.answers_url,
				"created_at": q.created_at					
			}
		}));
	} else {
		callback.e(cb, "Studio not found", null);
	}
}

Studios.findEssays = function findEssays(id, cb) {
	if(id >= 0 && id < studioDb.length) {
		callback.n(cb, null, db.essays().randomN(studioDb[id].essays).map(function(e) {
			return {
				"id": e.id,
				"title": e.title,
				"heat": e.heat,
				"supports": e.supports,
				"comments": e.comments,
				"url": e.url,
				"comments_url": e.comments_url,
				"created_at": e.created_at				
			};
		}));
	} else {
		callback.e(cb, "Studio not found", null);
	}
}

Studios.size = function size() {
	return studioDb.length;
}

Studios.randomN = function randomN(n) {
	if(!n) return [];

	var ret = undefined;
	var retIds = _.sample(_.range(0, studioDb.length - 1), n);
	Studios.find(retIds, function(err, studios) {
		ret = studios;
	});

	return ret;
}

Studios.saveOne = function saveOne(studio, user, cb) {
	var id = studioDb.length;

	db.users().findOneBy({ email: user.email }, function(err, manager) {
		if(err) {
			callback.e(cb, 'Manager not found', null);
		} else {
			var msOptions = [];

			if(studio.members) {
				studio.members.push(manager.email);
				msOptions = db.users().extractAccountOption(studio.members);
			} else {
				msOptions.push(db.users().extractAccountOption(manager.email));
			}

			db.users().findBy(msOptions, function(err, members) {
				var newStudio = {
					"id": id,
					"name": studio.name,
					"manager": manager.name,
					"members": members.length,
					"questions": 0,
					"essays": 0,
					"url": config.baseUrl + "/api/studios/" + id,
					"avatar_url": studio.avatar ? studio.avatar : (config.baseUrl + '/avatars/49.png'),
					"manager_url": manager.url,
					"manager_avatar_url": manager.avatar_url,
					"members_url": config.baseUrl + "/api/studios/" + id + "/members",
					"questions_url": config.baseUrl + "/api/studios/" + id + "/questions",
					"essays_url": config.baseUrl + "/api/studios/" + id + "/essays",
					"top_question_url": null,
					"top_essay_url": null,
					"bio": studio.bio ? studio.bio : ""
				};

				db.users().updateOne(manager, { studios: manager.studios + 1 }, function(err, newManager) {
					if(err) {
						callback.e(cb, 'Error when update manager', null);
					} else {
						studioDb.push(newStudio);
						saveStudioDb(studioDb);
						callback.n(cb, null, newStudio);
					}
				});
			})
		}
	});

}

Studios.updateOne = function updateOne(studio, options, cb) {
	var id = studio.id;
	if(id >= 0 && id < studioDb.length) {
		for(var key in options) {
			if(key === "members") {
				// we assume that every member is valid
				studioDb[id][key] = options[key].length;
			} else if(key in studio) {
				studioDb[id][key] = options[key];
				if(key === "manager") {
					studioDb[id].manager_url = options.newManager.url;
					studioDb[id].manager_avatar_url = options.newManager.avatar_url;
				}
			}
		}
		saveStudioDb(studioDb);
		callback.n(cb, null, studioDb[id]);
		return ;
	}
	callback.e(cb, 'Studio not found', null);
}

module.exports = Studios;