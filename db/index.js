module.exports = {
	// the reason why we defined them as functions is to handle cross-reference
	users: function() { return require('./users.js'); },
	studios: function() { return require('./studios.js'); },
	questions: function() { return require('./questions.js'); },
	essays: function() { return require('./essays.js'); },
	answers: function() { return require('./answers.js'); },
	comments: function() { return require('./comments.js'); }
};