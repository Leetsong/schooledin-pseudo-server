const _ = require('underscore');
const log = require('../util/log.js');
const callback = require('../util/callback.js');
const config = require('../jsondb/config.json');
const essayDb = require('../jsondb/essays.json');
const db = require('./index.js');

var Essays = { };

function saveEssayDb(savingDb) {
	require('fs').writeFileSync(
		require('path').join(__dirname, "../jsondb/essays.json"), JSON.stringify(savingDb));
}

Essays.find = function find(ids, cb) {
	var essays = [];
	for(var i = 0; i < ids.length; i ++) {
		if(ids[i] >= 0 && ids[i] < essayDb.length) {
			essays.push(_.omit(
				essayDb[ids[i]], 
				'content', 'studio_bio', 'studio_avatar_url'));
	  }
	}
	callback.n(cb, null, essays);
}

Essays.findOne = function findOne(id, cb) {
	if(id >= 0 && id < essayDb.length) {
		callback.n(cb, null, essayDb[id]);
	} else {
		callback.e(cb, "Essay not found", null);
	}
}

Essays.findComments = function findComments(id, cb) {
	if(id >= 0 && id < essayDb.length) {
		callback.n(cb, null, db.comments().randomN(essayDb[id].comments));
	} else {
		callback.e(cb, "Essay not found", null);
	}
}

Essays.size = function size() {
	return essayDb.length;
}

Essays.randomN = function randomN(n) {
	if(!n) return [];

	var ret = undefined;
	var retIds = _.sample(_.range(0, essayDb.length - 1), n);
	Essays.find(retIds, function(err, essays) {
		ret = essays;
	});

	return ret;
}

Essays.saveOne = function saveOne(essay, cb) {
	var id = essayDb.length;

	db.studios().findOneBy({ name: essay.studio }, function(err, studio) {
		if(err) {
			callback.e(cb, 'Studio not found', null);
		} else {
			var newEssay = {
				"id": id,
				"title": essay.essay_title,
				"content": essay.essay_content,
				"heat": 0,
				"supports": 0,
				"comments": 0,
				"studio": studio.name,
				"url": config.baseUrl + "/api/essays/" + id,
				"comments_url": config.baseUrl + "/api/essays/" + id + "/comments",
				"studio_url": studio.url,
				"studio_avatar_url": studio.avatar_url,
				"created_at": Date.now()
			};

			db.studios().updateOne(studio, { essays: studio.essays + 1 }, function(err, newStudio) {
				if(err) {
					callback.e(cb, 'Error when update studio', null);
				} else {
					essayDb.push(newEssay);
					saveEssayDb(essayDb);
					callback.n(cb, null, newEssay);
				}
			});
		}
	});

}

Essays.updateOne = function updateOne(essay, options, cb) {
	var id = essay.id;
	if(id >= 0 && id < essayDb.length) {
		for(var key in options) {
			if(key in essay) {
				essayDb[id][key] = options[key];
			}
		}
		saveEssayDb(essayDb);
		callback.n(cb, null, essayDb[id]);
		return ;
	}
	callback.e(cb, 'Essay not found', null);
}

module.exports = Essays;