const _ = require('underscore');
const log = require('../util/log.js');
const callback = require('../util/callback.js');
const config = require('../jsondb/config.json');
const questionDb = require('../jsondb/questions.json');
const db = require('./index.js');

var Questions = { };

function saveQuestionDb(savingDb) {
	require('fs').writeFileSync(
		require('path').join(__dirname, "../jsondb/questions.json"), JSON.stringify(savingDb));
}

Questions.find = function find(ids, cb) {
	var questions = [];
	for(var i = 0; i < ids.length; i ++) {
		if(ids[i] >= 0 && ids[i] < questionDb.length) {
			var q = questionDb[ids[i]];
			questions.push(_.omit(
				questionDb[ids[i]], 
				'content', 'directed_to_url'));
		}
	}
	callback.n(cb, null, questions);
}

Questions.findOne = function findOne(id, cb) {
	if(id >= 0 && id < questionDb.length) {
		callback.n(cb, null, questionDb[id]);
	} else {
		callback.e(cb, "Question not found", null);
	}
}

Questions.findAnswers = function findAnswers(id, cb) {
	if(id >= 0 && id < questionDb.length) {
		callback.n(cb, null, db.answers().randomN(questionDb[id].answers));
	} else {
		callback.e(cb, "Question not found", null);
	}
}

Questions.size = function size() {
	return questionDb.length;
}

Questions.randomN = function randomN(n) {
	if(!n) return [];
	
	var ret = undefined;
	var retIds = _.sample(_.range(0, questionDb.length - 1), n);
	Questions.find(retIds, function(err, questions) {
		ret = questions;
	});

	return ret;
}

Questions.saveOne = function saveOne(question, user, cb) {
	var id = questionDb.length;

	db.studios().findOneBy({ name: question.directed_to }, function(err, studio) {
		if(err) {
			callback.e(cb, 'Studio not found', null);
		} else {
			var newQuestion = {
				"id": id,
				"title": question.question_title,
				"content": question.question_content ? question.question_content : "",
				"heat": 0,
				"supports": 0,
				"answers": 0,
				"directed_to": studio.name,
				"url": config.baseUrl + "/api/questions/" + id,
				"answers_url": config.baseUrl + "/api/questions/" + id + "/answers",
				"directed_to_url": studio.url,
				"created_at": Date.now()
			};

			db.users().updateOne(user, { questions: user.questions + 1 }, function(err, newUser) {
				if(err) {
					callback.e(cb, 'Error when update user', null);
				} else {
					questionDb.push(newQuestion);
					saveQuestionDb(questionDb);
					callback.n(cb, null, newQuestion);
				}
			});
		}
	});

}

Questions.updateOne = function updateOne(question, options, cb) {
	var id = question.id;
	if(id >= 0 && id < questionDb.length) {
		for(var key in options) {
			if(key in question) {
				questionDb[id][key] = options[key];
			}
		}
		saveQuestionDb(questionDb);
		callback.n(cb, null, questionDb[id]);
		return ;
	}
	callback.e(cb, 'Question not found', null);
}

module.exports = Questions;