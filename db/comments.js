const _ = require('underscore');
const log = require('../util/log.js');
const callback = require('../util/callback.js');
const config = require('../jsondb/config.json');
const commentDb = require('../jsondb/comments.json');
const db = require('./index.js');

var Comments = { };

function saveCommentDb(savingDb) {
	require('fs').writeFileSync(
		require('path').join(__dirname, "../jsondb/comments.json"), JSON.stringify(savingDb));
}

Comments.findOne = function findOne(id, cb) {
	if(id >= 0 && id < commentDb.length) {
		callback.n(cb, null, commentDb[id]);
	} else {
		callback.e(cb, "Comment not found", null);
	}
}

Comments.size = function size() {
	return commentDb.length;
}

Comments.randomN = function randomN(n) {
	if(!n) return [];

	var ret = undefined;
	var retIds = _.sample(_.range(0, commentDb.length - 1), n);
	ret = retIds.map(function(id) {
		return commentDb[id];
	});

	return ret;
}

Comments.saveOne = function saveOne(comment, user, cb) {
	var p;
	if(comment.parent_type === "essay") {
		p = db.essays();
	} else if(comment.parent_type === "answer") {
		p = db.answers();
	} else {
		callback.e(cb, 'Invalid comment parent type', null);
		return ;
	}

	var id = commentDb.length;
	p.findOne(comment.parent_id, function(err, parent) {
		if(err) {
			callback.e(cb, 'Parent not found', null);
		} else {
			db.users().findOneBy({ email: user.email }, function(err, commenter) {
				if(err) {
					callback.e(cb, 'Commenter not found', null);
				} else {
					var newComment = {
						"id": id,
						"commenter": commenter.name,
						"content": comment.content,
						"url": config.baseUrl + "/api/comments/" + id,
						"commenter_url": commenter.url,
						"commenter_avatar_url": commenter.avatar_url,
						"parent_url": parent.url,
						"created_at": Date.now()
					};

					p.updateOne(parent, { comments: parent.comments + 1 }, function(err, newParent) {
						if(err) {
							callback.e(cb, 'Error when update parent', null);
						} else {
							commentDb.push(newComment);
							saveCommentDb(commentDb);
							callback.n(cb, null, newComment);
						}
					});
				}
			});
		}
	});

}

module.exports = Comments;