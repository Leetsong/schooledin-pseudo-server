const _ = require('underscore');
const log = require('../util/log.js');
const callback = require('../util/callback.js');
const config = require('../jsondb/config.json');
const answerDb = require('../jsondb/answers.json');
const db = require('./index.js');

var Answers = { };

function saveAnswerDb(savingDb) {
	require('fs').writeFileSync(
		require('path').join(__dirname, "../jsondb/answers.json"), JSON.stringify(savingDb));
}

Answers.findOne = function findOne(id, cb) {
	if(id >= 0 && id < answerDb.length) {
		callback.n(cb, null, answerDb[id]);
	} else {
		callback.e(cb, "Answer not found", null);
	}
}

Answers.findComments = function findComments(id, cb) {
	if(id >= 0 && id < answerDb.length) {
		callback.n(cb, null, db.comments().randomN(answerDb[id].comments));
	} else {
		callback.e(cb, "Answer not found", null);
	}
}

Answers.size = function size() {
	return answerDb.length;
}

Answers.randomN = function randomN(n) {
	if(!n) return [];

	var ret = undefined;
	var retIds = _.sample(_.range(0, answerDb.length - 1), n);
	ret = retIds.map(function(id) {
		return answerDb[id];
	});

	return ret;
}

Answers.updateOne = function updateOne(answer, options, cb) {
	var id = answer.id;
	if(id >= 0 && id < answerDb.length) {
		for(var key in options) {
			if(key in answer) {
				answerDb[id][key] = options[key];
			}
		}
		saveAnswerDb(answerDb);
		callback.n(cb, null, answerDb[id]);
		return ;
	}
	callback.e(cb, 'Answer not found', null);
}

Answers.saveOne = function saveOne(answer, user, cb) {
	var id = answerDb.length;
	
	db.questions().findOne(answer.question_id, function(err, question) {
		if(err) {
			callback.e(cb, 'Question not found', null);
		} else {
			db.users().findOneBy({ email: user.email }, function(err, answerer) {
				if(err) {
					callback.e(cb, 'Answerer not found', null);
				} else {
					var newAnswer = {
						"id": id,
						"answerer": answerer.name,
						"content": answer.content,
						"question_title": question.title,
						"comments": 0,
						"url": config.baseUrl + "/api/answers/" + id,
						"question_url": question.url,
						"answerer_url": answerer.url,
						"answerer_avatar_url": answerer.avatar_url,
						"comments_url": config.baseUrl + "/api/answers/" + id + "/comments",
						"created_at": Date.now()
					};

					db.questions().updateOne(question, { answers: question.answers + 1 }, function(err, newQuestion) {
						if(err) {
							callback.e(cb, 'Error when update question', null);
						} else {
							answerDb.push(newAnswer);
							saveAnswerDb(answerDb);
							callback.n(cb, null, newAnswer);
						}
					});
				}
			});
		}
	});

}

module.exports = Answers;