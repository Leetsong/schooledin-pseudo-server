const log = require('./util/log.js')

module.exports = function interceptor(req, res, next) {

log.i(req.method + " " + req.url);

next();

}