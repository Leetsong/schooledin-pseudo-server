const fs = require('fs');
const path = require('path');
const log = require('./util/log.js');
var generateUsers = require('./generator/users.js');
var generateStudios = require('./generator/studios.js');
var generateQuestions = require('./generator/questions.js');
var generateEssays = require('./generator/essays.js');
var generateAnswers = require('./generator/answers.js');
var generateComments = require('./generator/comments.js');
var generateNotifications = require('./generator/notifications.js');
var config = require('./generator/config.js');

// extract configurations
extractConfigsTo(config);

// here we generate fake data
log.i('USE: ' + config.baseUrl + '\n');
generateUsers(config.baseUrl);
generateStudios(config.baseUrl);
generateQuestions(config.baseUrl);
generateEssays(config.baseUrl);
generateAnswers(config.baseUrl);
generateComments(config.baseUrl);
generateNotifications(config.baseUrl);

// generate a configuration file
var fileName = path.join(__dirname, "./jsondb/config.json");
fs.writeFileSync(fileName, JSON.stringify(config));

function extractConfigsTo(conf) {
	var params = process.argv.slice(2);
	var reg = /^--(\w+)=(.+)$/;

	for(var i = 0, l = params.length; i < l; i ++) {
		var item = params[i];
		var t = reg.exec(item);
		var k = t[1], v = t[2];
		if(k in conf) {
			switch(k) {
				case 'port':
					if(v <= 1000 || v >= 65536) {
						log.e('Invalid port `' + v + '`, (1000, 65536) is allowed, default port `' + conf[k] + '` is used');
					} else {
						conf[k] = v;
					}					
					break;

				case 'ipaddr':
					var ipReg = /^(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])\.(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9]?[0-9])$/;
					if(!ipReg.test(v)) {
						log.e('Invalid ip address `' + v + '`, leading 0s are not allowed, default ip address `' + conf[v] + '` is used');
					}
					else {
						conf[k] = v;
					}
					break;
			}
		}
	}

	conf.baseUrl = 'https://' + conf.ipaddr + ':' + conf.port;

	return conf;
}
